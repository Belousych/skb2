import express from 'express';
import cors from 'cors';

import canonize from './canonize.js'

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

// Урок 2 задание A
app.get('/task2A/', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

// Урок 2 задание B
app.get('/task2B/', (req, res) => {
  const fullname = req.query.fullname.toString();
  if (/[0-9]/ig.test(fullname)) {
    res.send('Invalid fullname');
  }
  const fullnameArr = fullname.split(/\s|_/);
  console.log(fullnameArr);
  const family = fullnameArr[fullnameArr.length - 1].toString();
  let result = family;
  if (fullnameArr.length > 3 || result[0] === undefined) {
    res.send('Invalid fullname');
  }

  if (fullnameArr.length > 1) {
      result = result + ` ${fullnameArr[0][0]}.`;
  }
  if (fullnameArr.length === 3) {
    result = result + ` ${fullnameArr[1][0]}.`
  }
  res.send(result);
});

// Урок 2 задание C
app.get('/task2C/', (req, res) => {
  const username = canonize(req.query.username);
  res.send(username);
});
app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
